package co.id.nexchief.dto;

import java.util.Date;

public class ProductDto {
	private int id;
	private String productCode;
	private String productName;
	private String packaging;
	private String productDescription;
	private String productCategory;
	private Date marketLaunchDate;
	private boolean productStatus;
	private double standardBuyingPrice;
	private double standardSellingPrice;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	
	public ProductDto() {
	}
	
	public ProductDto(String productCode, String productName, double standardSellingPrice) {
		this.productCode = productCode;
		this.productName = productName;
		this.standardSellingPrice = standardSellingPrice;
	}
	
	public ProductDto(String productCode, String productName, String packaging, String productDescription, String productCategory, Date marketLaunchDate, double standardBuyingPrice, double standardSellingPrice) {
		this.productCode = productCode;
		this.productName = productName;
		this.packaging = packaging;
		this.productDescription = productDescription;
		this.productCategory = productCategory;
		this.marketLaunchDate = marketLaunchDate;
		this.standardBuyingPrice = standardBuyingPrice;
		this.standardSellingPrice = standardSellingPrice;
	}
	
	public ProductDto(String productCode, String productName, String packaging,
	                  String productDescription, String productCategory, Date marketLaunchDate,
	                  boolean productStatus, double standardBuyingPrice, double standardSellingPrice) {
		this.productCode = productCode;
		this.productName = productName;
		this.packaging = packaging;
		this.productDescription = productDescription;
		this.productCategory = productCategory;
		this.marketLaunchDate = marketLaunchDate;
		this.productStatus = productStatus;
		this.standardBuyingPrice = standardBuyingPrice;
		this.standardSellingPrice = standardSellingPrice;
	}
	
	public ProductDto(String productCode, String productName, String packaging, String productDescription,
	                  String productCategory, Date marketLaunchDate, boolean productStatus, double standardBuyingPrice,
	                  double standardSellingPrice, Date createdAt, String createdBy, Date updatedAt, String updatedBy) {
		this.productCode = productCode;
		this.productName = productName;
		this.packaging = packaging;
		this.productDescription = productDescription;
		this.productCategory = productCategory;
		this.marketLaunchDate = marketLaunchDate;
		this.productStatus = productStatus;
		this.standardBuyingPrice = standardBuyingPrice;
		this.standardSellingPrice = standardSellingPrice;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
	}
	
	public ProductDto(int id, String productCode, String productName, double standardBuyingPrice, double standardSellingPrice) {
		this.id = id;
		this.productCode = productCode;
		this.productName = productName;
		this.standardBuyingPrice = standardBuyingPrice;
		this.standardSellingPrice = standardSellingPrice;
	}
	
	public ProductDto(int id, String productCode, String productName, Date marketLaunchDate, double standardBuyingPrice, double standardSellingPrice) {
		this.id = id;
		this.productCode = productCode;
		this.productName = productName;
		this.marketLaunchDate = marketLaunchDate;
		this.standardBuyingPrice = standardBuyingPrice;
		this.standardSellingPrice = standardSellingPrice;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getProductCode() {
		return productCode;
	}
	
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getPackaging() {
		return packaging;
	}
	
	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}
	
	public String getProductDescription() {
		return productDescription;
	}
	
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	
	public String getProductCategory() {
		return productCategory;
	}
	
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	
	public Date getMarketLaunchDate() {
		return marketLaunchDate;
	}
	
	public void setMarketLaunchDate(Date marketLaunchDate) {
		this.marketLaunchDate = marketLaunchDate;
	}
	
	public boolean isProductStatus() {
		return productStatus;
	}
	
	public void setProductStatus(boolean productStatus) {
		this.productStatus = productStatus;
	}
	
	public double getStandardBuyingPrice() {
		return standardBuyingPrice;
	}
	
	public void setStandardBuyingPrice(double standardBuyingPrice) {
		this.standardBuyingPrice = standardBuyingPrice;
	}
	
	public double getStandardSellingPrice() {
		return standardSellingPrice;
	}
	
	public void setStandardSellingPrice(double standardSellingPrice) {
		this.standardSellingPrice = standardSellingPrice;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getUpdatedBy() {
		return updatedBy;
	}
	
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}
