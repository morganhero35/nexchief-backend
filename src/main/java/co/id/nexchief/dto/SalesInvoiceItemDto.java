package co.id.nexchief.dto;

import java.util.Date;

public class SalesInvoiceItemDto {
	private int id;
	private int quantity;
	private double subtotal;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private ProductDto product;
	
	public SalesInvoiceItemDto(int id, int quantity, double subtotal) {
		this.id = id;
		this.quantity = quantity;
		this.subtotal = subtotal;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public double getSubtotal() {
		return subtotal;
	}
	
	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getUpdatedBy() {
		return updatedBy;
	}
	
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public ProductDto getProduct() {
		return product;
	}
	
	public void setProduct(ProductDto product) {
		this.product = product;
	}
}
