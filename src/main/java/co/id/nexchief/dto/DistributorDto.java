package co.id.nexchief.dto;

public class DistributorDto {
	private int id;
	private String distributorId;
	private String distributorName;
	private String address;
	private String city;
	private String ownerFirstname;
	private String ownerLastname;
	private String country;
	private String email;
	private String phone;
	private String fax;
	private String contactFirstname;
	private String contactLastname;
	private String contactPhone;
	private String contactEmail;
	private String website;
	
	public DistributorDto() {
	}
	
	public DistributorDto(String distributorId, String distributorName, String address, String city,
	                      String ownerFirstname, String ownerLastname, String country, String email,
	                      String phone, String fax, String contactFirstname, String contactLastname,
	                      String contactPhone, String contactEmail, String website) {
		this.distributorId = distributorId;
		this.distributorName = distributorName;
		this.address = address;
		this.city = city;
		this.ownerFirstname = ownerFirstname;
		this.ownerLastname = ownerLastname;
		this.country = country;
		this.email = email;
		this.phone = phone;
		this.fax = fax;
		this.contactFirstname = contactFirstname;
		this.contactLastname = contactLastname;
		this.contactPhone = contactPhone;
		this.contactEmail = contactEmail;
		this.website = website;
	}
	
	public DistributorDto(int id, String distributorId, String distributorName) {
		this.id = id;
		this.distributorId = distributorId;
		this.distributorName = distributorName;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDistributorId() {
		return distributorId;
	}
	
	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}
	
	public String getDistributorName() {
		return distributorName;
	}
	
	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getOwnerFirstname() {
		return ownerFirstname;
	}
	
	public void setOwnerFirstname(String ownerFirstname) {
		this.ownerFirstname = ownerFirstname;
	}
	
	public String getOwnerLastname() {
		return ownerLastname;
	}
	
	public void setOwnerLastname(String ownerLastname) {
		this.ownerLastname = ownerLastname;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getFax() {
		return fax;
	}
	
	public void setFax(String fax) {
		this.fax = fax;
	}
	
	public String getContactFirstname() {
		return contactFirstname;
	}
	
	public void setContactFirstname(String contactFirstname) {
		this.contactFirstname = contactFirstname;
	}
	
	public String getContactLastname() {
		return contactLastname;
	}
	
	public void setContactLastname(String contactLastname) {
		this.contactLastname = contactLastname;
	}
	
	public String getContactPhone() {
		return contactPhone;
	}
	
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	
	public String getContactEmail() {
		return contactEmail;
	}
	
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	
	public String getWebsite() {
		return website;
	}
	
	public void setWebsite(String website) {
		this.website = website;
	}
}
