package co.id.nexchief.dto;

import java.util.Date;

public class PrincipalDto {
	private String principalId;
	private String principalName;
	private String address;
	private String city;
	private String country;
	private String email;
	private String phone;
	private String fax;
	private String contactFirstname;
	private String contactLastname;
	private String contactPhone;
	private String contactEmail;
	private Date licensedExpiryDate;
	private Date deletedAt;
	
	public PrincipalDto(String principalId, String principalName, String address, String city, String country,
	                    String email, String phone, String fax, String contactFirstname,
	                    String contactLastname, String contactPhone, String contactEmail, Date licensedExpiryDate) {
		this.principalId = principalId;
		this.principalName = principalName;
		this.address = address;
		this.city = city;
		this.country = country;
		this.email = email;
		this.phone = phone;
		this.fax = fax;
		this.contactFirstname = contactFirstname;
		this.contactLastname = contactLastname;
		this.contactPhone = contactPhone;
		this.contactEmail = contactEmail;
		this.licensedExpiryDate = licensedExpiryDate;
	}
	
	public PrincipalDto(String principalId, String principalName, Date deletedAt) {
		this.principalId = principalId;
		this.principalName = principalName;
		this.deletedAt = deletedAt;
	}
	
	public String getPrincipalId() {
		return principalId;
	}
	
	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}
	
	public String getPrincipalName() {
		return principalName;
	}
	
	public void setPrincipalName(String principalName) {
		this.principalName = principalName;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getFax() {
		return fax;
	}
	
	public void setFax(String fax) {
		this.fax = fax;
	}
	
	public String getContactFirstname() {
		return contactFirstname;
	}
	
	public void setContactFirstname(String contactFirstname) {
		this.contactFirstname = contactFirstname;
	}
	
	public String getContactLastname() {
		return contactLastname;
	}
	
	public void setContactLastname(String contactLastname) {
		this.contactLastname = contactLastname;
	}
	
	public String getContactPhone() {
		return contactPhone;
	}
	
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	
	public String getContactEmail() {
		return contactEmail;
	}
	
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	
	public Date getLicensedExpiryDate() {
		return licensedExpiryDate;
	}
	
	public void setLicensedExpiryDate(Date licensedExpiryDate) {
		this.licensedExpiryDate = licensedExpiryDate;
	}
	
	public Date getDeletedAt() {
		return deletedAt;
	}
	
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
}
