package co.id.nexchief.dto;

import co.id.nexchief.model.Distributor;
import co.id.nexchief.model.Principals;

import java.util.Date;

public class UserDto {
	private int id;
	private String userId;
	private String name;
	private String email;
	private String roles;
	private String status;
	private boolean disableLogin;
	private Date productValidThru;
	private Date deletedAt;
	private Principals principal;
	private Distributor distributor;
	private String jwt;
	
	public UserDto() {
	}
	
	public UserDto(String userId, String name, String email, String role, String status, boolean disableLogin, Date productValidThru) {
		this.userId = userId;
		this.name = name;
		this.email = email;
		this.roles = role;
		this.status = status;
		this.disableLogin = disableLogin;
		this.productValidThru = productValidThru;
	}
	
	public UserDto(String userId, String name, String email, String roles, String status,
	               boolean disableLogin, Date productValidThru, Date deletedAt, Principals principal) {
		this.userId = userId;
		this.name = name;
		this.email = email;
		this.roles = roles;
		this.status = status;
		this.disableLogin = disableLogin;
		this.productValidThru = productValidThru;
		this.deletedAt = deletedAt;
		this.principal = principal;
	}
	
	public UserDto(int id, String userId, String name) {
		this.id = id;
		this.userId = userId;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getRoles() {
		return roles;
	}
	
	public void setRoles(String roles) {
		this.roles = roles;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public boolean isDisableLogin() {
		return disableLogin;
	}
	
	public void setDisableLogin(boolean disableLogin) {
		this.disableLogin = disableLogin;
	}
	
	public Date getProductValidThru() {
		return productValidThru;
	}
	
	public void setProductValidThru(Date productValidThru) {
		this.productValidThru = productValidThru;
	}
	
	public String getJwt() {
		return jwt;
	}
	
	public void setJwt(String jwt) {
		this.jwt = jwt;
	}
	
	public Principals getPrincipal() {
		return principal;
	}
	
	public void setPrincipal(Principals principal) {
		this.principal = principal;
	}
	
	public Date getDeletedAt() {
		return deletedAt;
	}
	
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
}
