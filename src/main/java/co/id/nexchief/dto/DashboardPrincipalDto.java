package co.id.nexchief.dto;

import java.util.Date;

public class DashboardPrincipalDto {
	private String principalId;
	private String principalName;
	private String address;
	private String city;
	private Date licensedExpiryDate;
	
	public DashboardPrincipalDto(String principalId, String principalName, String address, String city) {
		this.principalId = principalId;
		this.principalName = principalName;
		this.address = address;
		this.city = city;
	}
	
	public DashboardPrincipalDto(String principalId, String principalName, String address, String city, Date licensedExpiryDate) {
		this.principalId = principalId;
		this.principalName = principalName;
		this.address = address;
		this.city = city;
		this.licensedExpiryDate = licensedExpiryDate;
	}
	
	public String getPrincipalId() {
		return principalId;
	}
	
	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}
	
	public String getPrincipalName() {
		return principalName;
	}
	
	public void setPrincipalName(String principalName) {
		this.principalName = principalName;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public Date getLicensedExpiryDate() {
		return licensedExpiryDate;
	}
	
	public void setLicensedExpiryDate(Date licensedExpiryDate) {
		this.licensedExpiryDate = licensedExpiryDate;
	}
}
