package co.id.nexchief.dto;

import java.util.Date;
import java.util.List;

public class SalesInvoiceDto {
	private int id;
	private Date transactionDate;
	private String customer;
	private double discount;
	private double tax;
	private double invoice;
	private double totalItem;
	private String transactionStatus;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private List<SalesInvoiceItemDto> salesInvoiceItems;
	private UserDto user;
	private DistributorDto distributor;
	
	public SalesInvoiceDto(int id, Date transactionDate, String customer, double invoice, double totalItem, String transactionStatus) {
		this.id = id;
		this.transactionDate = transactionDate;
		this.customer = customer;
		this.invoice = invoice;
		this.totalItem = totalItem;
		this.transactionStatus = transactionStatus;
	}
	
	public SalesInvoiceDto(Date transactionDate, String customer, double discount, double tax, double invoice, double totalItem, String transactionStatus) {
		this.transactionDate = transactionDate;
		this.customer = customer;
		this.discount = discount;
		this.tax = tax;
		this.invoice = invoice;
		this.totalItem = totalItem;
		this.transactionStatus = transactionStatus;
	}
	
	public SalesInvoiceDto(int id, Date transactionDate, String customer, double discount, double tax, double invoice, double totalItem, String transactionStatus) {
		this.id = id;
		this.transactionDate = transactionDate;
		this.customer = customer;
		this.discount = discount;
		this.tax = tax;
		this.invoice = invoice;
		this.totalItem = totalItem;
		this.transactionStatus = transactionStatus;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Date getTransactionDate() {
		return transactionDate;
	}
	
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	public String getCustomer() {
		return customer;
	}
	
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	
	public double getDiscount() {
		return discount;
	}
	
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	public double getTax() {
		return tax;
	}
	
	public void setTax(double tax) {
		this.tax = tax;
	}
	
	public double getInvoice() {
		return invoice;
	}
	
	public void setInvoice(double invoice) {
		this.invoice = invoice;
	}
	
	public double getTotalItem() {
		return totalItem;
	}
	
	public void setTotalItem(double totalItem) {
		this.totalItem = totalItem;
	}
	
	public String getTransactionStatus() {
		return transactionStatus;
	}
	
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getUpdatedBy() {
		return updatedBy;
	}
	
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public List<SalesInvoiceItemDto> getSalesInvoiceItems() {
		return salesInvoiceItems;
	}
	
	public void setSalesInvoiceItems(List<SalesInvoiceItemDto> salesInvoiceItems) {
		this.salesInvoiceItems = salesInvoiceItems;
	}
	
	public UserDto getUser() {
		return user;
	}
	
	public void setUser(UserDto user) {
		this.user = user;
	}
	
	public DistributorDto getDistributor() {
		return distributor;
	}
	
	public void setDistributor(DistributorDto distributor) {
		this.distributor = distributor;
	}
}
