package co.id.nexchief.dto;

import java.util.Date;

public class DashboardUserDto {
	private String userId;
	private String name;
	private Date productValidThru;
	
	public DashboardUserDto(String userId, String name, Date productValidThru) {
		this.userId = userId;
		this.name = name;
		this.productValidThru = productValidThru;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getProductValidThru() {
		return productValidThru;
	}
	
	public void setProductValidThru(Date productValidThru) {
		this.productValidThru = productValidThru;
	}
}
