package co.id.nexchief.dto;

public class DashboardDistributorDto {
	private String distributorId;
	private String distributorName;
	private String address;
	private String city;
	
	public DashboardDistributorDto(String distributorId, String distributorName, String address, String city) {
		this.distributorId = distributorId;
		this.distributorName = distributorName;
		this.address = address;
		this.city = city;
	}
	
	public String getDistributorId() {
		return distributorId;
	}
	
	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}
	
	public String getDistributorName() {
		return distributorName;
	}
	
	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
}
