package co.id.nexchief.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;
	@NotNull
	@Column(unique = true)
	private String userId;
	@NotNull
	@Pattern(regexp = "^[\\p{L} .'-]+$", message = "Full Name Only Alphabet!")
	private String name;
	@NotNull
	@Pattern(regexp = "^(?=.*\\d)(?=.*[A-Z])(?=.*[a-z]).{8,}$", message = "Minimum 8 characters, " +
			                                                                      "at least one uppercase letter, " +
			                                                                      "one lowercase letter, one number.")
	private String password;
	@Lob
	@NotNull
	private String address;
	@NotNull
	@Pattern(regexp = "^(?=.*\\d.*).{9,15}$", message = "Phone Number must be Number or Phone Number 9 - 15 Digits")
	private String phone;
	@NotNull
	@Column(unique = true)
	@Pattern(regexp = "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
			                  + "(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9]"
			                  + "(?:[A-Za-z0-9-]*[A-Za-z0-9])?", message = "email not valid, Example: " +
					                                                               "info@nexsoft.co.id")
	private String email;
	private String status;
	private boolean disableLogin;
	private String roles;
	private Date registrationDate;
	private Date productValidThru;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private Date deletedAt;
	
	@ManyToOne
	@NotNull
	private Principals principal;
	
	@ManyToOne
	private Distributor distributor;
	
}
