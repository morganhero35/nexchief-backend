package co.id.nexchief.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SalesInvoiceItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int quantity;
	private double subtotal;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	@ManyToOne
	private Product product;
	
	@ManyToOne(targetEntity = SalesInvoice.class)
	@JsonIgnore
	private SalesInvoice salesInvoice;
	
}
