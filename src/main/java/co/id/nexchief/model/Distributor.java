package co.id.nexchief.model;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Distributor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;
	@Column(unique = true)
	private String distributorId;
	@NotNull
	private String distributorName;
	@Lob
	@NotNull
	private String address;
	@NotNull
	private String city;
	
	@NotNull
	@Pattern(regexp = "^[\\p{L} .'-]+$", message = "Owner First Name Only Alphabet!")
	private String ownerFirstname;
	private String ownerLastname;
	
	private String country;
	
	@NotNull
	@Pattern(regexp = "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\." + "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
			                  + "(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9]"
			                  + "(?:[A-Za-z0-9-]*[A-Za-z0-9])?", message = "email not valid, Example: info@nexsoft.co.id")
	private String email;
	
	@NotNull
	@Pattern(regexp = "^(?=.*\\d.*).{9,15}$", message = "Phone Number must be Number or Phone Number 9 - 15 Digits")
	private String phone;
	
	private String fax;
	
	private String contactFirstname;
	private String contactLastname;
	private String contactPhone;
	private String contactEmail;
	
	private String website;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private Date deletedAt;
	
	@ManyToOne
	@JoinColumn(name = "principal_id")
	private Principals principal;
}
