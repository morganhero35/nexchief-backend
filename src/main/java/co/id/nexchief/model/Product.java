package co.id.nexchief.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;
	@Column(unique = true)
	@NotNull
	private String productCode;
	@NotNull
	private String productName;
	@NotNull
	private String packaging;
	@Lob
	private String productDescription;
	@NotNull
	private String productCategory;
	private Date marketLaunchDate;
	private boolean productStatus;
	private double standardBuyingPrice;
	private double standardSellingPrice;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private Date deletedAt;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
}
