package co.id.nexchief.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Principals {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(unique = true)
	private String principalId;
	@NotNull
	private String principalName;
	@Lob
	@NotNull
	private String address;
	@NotNull
	private String city;
	private String country;
	@NotNull
	@Pattern(regexp = "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\." + "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
			                  + "(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9]"
			                  + "(?:[A-Za-z0-9-]*[A-Za-z0-9])?", message = "email not valid, Example: info@nexsoft.co.id")
	private String email;
	@NotNull
	@Pattern(regexp = "^(?=.*\\d.*).{9,15}$", message = "Phone Number must be Number or Phone Number 9 - 15 Digits")
	private String phone;
	private String fax;
	private String contactFirstname;
	private String contactLastname;
	private String contactPhone;
	private String contactEmail;
	private Date licensedExpiryDate;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private Date deletedAt;
	
}
