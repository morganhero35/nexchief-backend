package co.id.nexchief.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SalesInvoice {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Date transactionDate;
	@NotNull
	private String customer;
	private double discount;
	private double tax;
	private double invoice;
	private double totalItem;
	private String transactionStatus;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private Date deletedAt;
	
	@NotNull
	@OneToMany(targetEntity = SalesInvoiceItem.class, cascade = CascadeType.ALL, mappedBy = "salesInvoice")
	private List<SalesInvoiceItem> salesInvoiceItems;
	
	@NotNull
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private User user;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "distributor_id")
	private Distributor distributor;
}
