package co.id.nexchief.repository;

import co.id.nexchief.dto.ProductDto;
import co.id.nexchief.model.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
	
	@Query(value = "SELECT new co.id.nexchief.dto.ProductDto(p.productCode, p.productName, p.packaging, " +
			               "p.productDescription, p.productCategory, p.marketLaunchDate, p.productStatus," +
			               "p.standardBuyingPrice,p.standardSellingPrice) " +
			               "FROM Product p " +
			               "WHERE p.productStatus = true AND p.deletedAt IS NULL AND p.user.userId LIKE %:id% ")
	List<ProductDto> findAllProductByUserId(String id);

//		@Query(value = "SELECT product_code,product_name, standard_selling_price " +
//			               "FROM product " +
//			               "WHERE product_status = true AND deleted_at IS NULL AND user_id LIKE = ?1 ", nativeQuery = true)
//	List<Product> findAllProductSalesByUserId(int id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.ProductDto(p.productCode, p.productName, p.standardSellingPrice) " +
			               "FROM Product p " +
			               "WHERE p.productStatus = true AND p.deletedAt IS NULL AND p.productStatus = TRUE AND p.user.userId LIKE %:id% ")
	List<ProductDto> findAllProductSalesByUserId(String id);
	
	@Query(value = "SELECT id, product_code, product_name, packaging, product_description, product_category, " +
			               "market_launch_date, product_status, standard_buying_price, standard_selling_price, " +
			               "created_at, created_by, updated_at, updated_by, deleted_at, user_id " +
			               "FROM product " +
			               "WHERE product_code LIKE :code", nativeQuery = true)
	Product findProductByProductCode(String code);
	
	@Query(value = "SELECT new co.id.nexchief.dto.ProductDto(p.productCode, p.productName, p.packaging, " +
			               "p.productDescription, p.productCategory, p.marketLaunchDate, p.productStatus, " +
			               "p.standardBuyingPrice,p.standardSellingPrice, p.createdAt, p.createdBy, p.updatedAt, " +
			               "p.updatedBy) FROM Product p WHERE p.productCode = :id")
	ProductDto findProductDtoByProductCode(String id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.ProductDto(p.productCode, p.productName, p.packaging, " +
			               "p.productDescription, p.productCategory, p.marketLaunchDate, " +
			               "p.standardBuyingPrice,p.standardSellingPrice) FROM Product p WHERE p.user.userId = :id")
	List<ProductDto> productDtoReport(String id);
	
	
	@Query(value = "SELECT new co.id.nexchief.dto.ProductDto(p.id, p.productCode, p.productName, p.marketLaunchDate,  p.standardBuyingPrice, p.standardSellingPrice) " +
			               "FROM Product p  WHERE (p.productName LIKE %:name% OR p.productCode LIKE %:name% " +
			               "OR p.productCategory LIKE %:name%) AND p.user.userId = :id AND p.deletedAt IS NULL")
	List<ProductDto> searchProducts(String name, String id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE product p SET p.product_name = :#{#product.productName}, p.packaging = :#{#product.packaging}," +
			               "p.product_description = :#{#product.productDescription}, p.product_category = :#{#product.productCategory}, " +
			               "p.market_launch_date = :#{#product.marketLaunchDate}, p.product_status = :#{#product.productStatus}, " +
			               "p.standard_buying_price = :#{#product.standardBuyingPrice}, " +
			               "p.standard_selling_price = :#{#product.standardSellingPrice},p.created_at = :#{#product.createdAt}, " +
			               "p.created_by = :#{#product.createdBy}, p.updated_at = :#{#product.updatedAt}," +
			               "p.updated_by = :#{#product.updatedBy}, p.user_id = :#{#product.user} WHERE p.product_code = :id",
			nativeQuery = true)
	void updateProduct(@Param("product") Product product, @Param("id") String id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE Product p SET p.deletedAt = CURRENT_DATE , p.updatedAt = CURRENT_DATE , " +
			               "p.updatedBy = :#{#product.updatedBy} WHERE p.id = :#{#product.id}")
	void deletedProduct(@Param("product") Product product);
	
	@Query("SELECT new co.id.nexchief.dto.ProductDto(p.id, p.productCode, p.productName, p.marketLaunchDate, p.standardBuyingPrice, " +
			       "p.standardSellingPrice) " +
			       "FROM Product p " +
			       "WHERE p.user.userId = :id AND p.deletedAt IS NULL")
	List<ProductDto> ProductDtoPage(String id, Pageable pageable);
	
	@Query(value = "SELECT count(p.id) FROM Product p WHERE p.user.userId = :id AND p.deletedAt IS NULL")
	int countProduct(String id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.ProductDto(p.id, p.productCode, p.productName, " +
			               "p.standardBuyingPrice, p.standardSellingPrice) " +
			               "FROM Product p JOIN SalesInvoiceItem s ON p.id = s.product.id " +
			               "WHERE s.id=:id")
	ProductDto findProductDtoBySalesInvoiceItem(int id);
}
