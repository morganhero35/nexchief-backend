package co.id.nexchief.repository;

import co.id.nexchief.dto.DashboardUserDto;
import co.id.nexchief.dto.UserDto;
import co.id.nexchief.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	@Query(value = "SELECT id, user_id, name, password, address, phone, email, status, disable_login, roles, " +
			               "registration_date, product_valid_thru, created_at, created_by, updated_at, " +
			               "updated_by, deleted_at, principal_id, distributor_id " +
			               "FROM user " +
			               "WHERE deleted_at IS NULL " +
			               "AND roles LIKE 'ROLE_USER' " +
			               "AND u.productValidThru > CURRENT_TIMESTAMP", nativeQuery = true)
	List<User> findAll();
	
	@Query(value = "SELECT id, user_id, name, password, address, phone, email, status, disable_login, roles, " +
			               "registration_date, product_valid_thru, created_at, created_by, updated_at," +
			               " updated_by, deleted_at, principal_id, distributor_id " +
			               "FROM user " +
			               "WHERE user_id = :id AND deleted_at IS NULL", nativeQuery = true)
	User findByUserId(String id);
	@Query(value = "SELECT id, user_id, name, password, address, phone, email, status, disable_login, roles, " +
			               "registration_date, product_valid_thru, created_at, created_by, updated_at," +
			               " updated_by, deleted_at, principal_id, distributor_id " +
			               "FROM user " +
			               "WHERE user_id = :id AND deleted_at IS NULL", nativeQuery = true)
	User findByUserIdCheck(String id);
	
	@Query(value = "SELECT id, user_id, name, password, address, phone, email, status, disable_login, roles, " +
			               "registration_date, product_valid_thru, created_at, created_by, updated_at," +
			               " updated_by, deleted_at, principal_id, distributor_id FROM user WHERE user_id = :id", nativeQuery = true)
	User findByUserIdLogin(String id);
	
	
	@Query(value = "SELECT id, user_id, name, password, address, phone, email, status, disable_login, roles, " +
			               "registration_date, product_valid_thru, created_at, created_by, updated_at, updated_by, " +
			               "deleted_at, principal_id, distributor_id FROM user WHERE email LIKE :email", nativeQuery = true)
	User findByEmail(String email);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE user SET deleted_at = :#{#user.deletedAt} WHERE user_id = :id", nativeQuery = true)
	void deleteById(@Param("user") User user, @Param("id") String id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE user u SET u.name = :#{#user.name}, u.email = :#{#user.email}, u.status = :#{#user.status}, u.phone = :#{#user.phone}," +
			               "u.address = :#{#user.address}, u.disable_login = :#{#user.disableLogin}, u.product_valid_thru = :#{#user.productValidThru}, " +
			               "u.updated_at = :#{#user.updatedAt},u.updated_by = :#{#user.updatedBy} WHERE u.user_id = :id",
			nativeQuery = true)
	void updateUser(@Param("user") User user, @Param("id") String id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE user u SET u.password = :#{#user.password}, " +
			               "u.updated_at = :#{#user.updatedAt}, u.updated_by = :#{#user.updatedBy} WHERE u.user_id = :id",
			nativeQuery = true)
	void updatePasswordUser(@Param("user") User user, @Param("id") String id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.UserDto(u.userId, u.name,u.email,u.roles,u.status," +
			               "u.disableLogin,u.productValidThru) FROM User u WHERE u.email = :id")
	UserDto findUserDtoByEmail(String id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.UserDto(u.userId, u.name,u.email,u.roles,u.status," +
			               "u.disableLogin,u.productValidThru, u.deletedAt, u.principal) " +
			               "FROM User u JOIN u.principal " +
			               "WHERE u.userId = :id")
	UserDto findUserDtoByUserId(String id);
	
	@Query(value = "SELECT count(u.id) FROM User u " +
			               "WHERE u.deletedAt IS NULL AND u.roles LIKE 'ROLE_USER' ")
	int countUser();
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardUserDto(u.userId, u.name, u.productValidThru) FROM User u " +
			               "WHERE u.deletedAt IS NULL  AND u.roles LIKE 'ROLE_USER'")
	List<DashboardUserDto> pageUser(Pageable pageable);
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardUserDto(u.userId, u.name, u.productValidThru) " +
			               "FROM User u WHERE (u.name LIKE %:name% OR u.userId LIKE %:name%) AND u.deletedAt IS NULL AND u.roles LIKE 'ROLE_USER'")
	List<DashboardUserDto> searchUser(@Param("name") String name);
	
	@Query(value = "SELECT count (u.id) FROM User u WHERE u.principal.principalId LIKE :id")
	int countUserByPrincipalId(@Param("id") String id);
	
	@Query(value = "SELECT u FROM User u WHERE u.principal.principalId LIKE :id")
	List<User> findUserByPrincipalId(@Param("id") String id);
	
	@Query(value = "SELECT count (u) FROM User u WHERE u.distributor.distributorId LIKE :id")
	int countUserByDistributorId(@Param("id") String id);
	
	//	@Query(value = "SELECT new co.id.nexchief.model.User(u.id, u.userId, u.name, u.password) FROM User u WHERE u.distributor.distributorId LIKE :id")
//	List<User> findUserByDistributorId(@Param("id") String id);
//
	@Query(value = "SELECT u FROM User u WHERE u.distributor.distributorId LIKE :id")
	List<User> findUserByDistributorId(@Param("id") String id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE user SET deleted_at = now() WHERE id = :id", nativeQuery = true)
	void deletedUser(@Param("id") int id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.UserDto(u.id, u.userId, u.name) FROM User u JOIN SalesInvoice s ON s.user.id = u.id WHERE s.id = :id")
	UserDto findUserByIdSales(int id);
}
