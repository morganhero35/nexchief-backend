package co.id.nexchief.repository;

import co.id.nexchief.dto.DashboardDistributorDto;
import co.id.nexchief.dto.DistributorDto;
import co.id.nexchief.model.Distributor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DistributorRepository extends JpaRepository<Distributor, Integer> {
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardDistributorDto(d.distributorId,d.distributorName,d.address,d.city) " +
			               "FROM Distributor d " +
			               "WHERE d.deletedAt IS NULL")
	List<DashboardDistributorDto> findAllDistributor();
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardDistributorDto(d.distributorId,d.distributorName,d.address,d.city) " +
			               "FROM Distributor d " +
			               "WHERE d.deletedAt IS NULL")
	List<DashboardDistributorDto> findAllDistributorPage(Pageable pageable);
	
	@Query(value = "SELECT id, distributor_id, distributor_name, address, city, owner_firstname, owner_lastname, " +
			               "country, email, phone, fax, contact_firstname, contact_lastname, contact_phone, contact_email, " +
			               "website,created_at, created_by, updated_at, updated_by, deleted_at, principal_id " +
			               "FROM distributor  WHERE deleted_at IS null AND distributor_id = :id", nativeQuery = true)
	Distributor findDistributorById(String id);
	
	@Query(value = "SELECT id, distributor_id, distributor_name, address, city, owner_firstname, owner_lastname, " +
			               "country, email, phone, fax, contact_firstname, contact_lastname, contact_phone, contact_email, " +
			               "website,created_at, created_by, updated_at, updated_by, deleted_at, principal_id " +
			               "FROM distributor  WHERE deleted_at IS null AND distributor_id = :id", nativeQuery = true)
	Distributor findDistributorByIdCheck(String id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardDistributorDto(d.distributorId,d.distributorName," +
			               "d.address,d.city) " +
			               "FROM Distributor d WHERE (d.distributorName LIKE %:name% " +
			               "OR d.distributorId LIKE %:name%) AND d.deletedAt IS NULL")
	List<DashboardDistributorDto> searchDistributor(String name);
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardDistributorDto(d.distributorId,d.distributorName," +
			               "d.address,d.city) " +
			               "FROM Distributor d WHERE d.distributorId LIKE %:id%  AND d.deletedAt IS NULL")
	DashboardDistributorDto findDistributorDtoById(String id);
	
	@Query(value = "SELECT count(d.id) FROM Distributor d WHERE d.deletedAt IS NULL")
	int countDistributor();
	
	@Query(value = "SELECT count(d.id) FROM Distributor d WHERE d.principal.principalId LIKE :id AND d.deletedAt IS NULL ")
	int countDistributorByIdPrincipal(@Param("id") String id);
	
	@Query(value = "SELECT count(d.id) FROM Distributor d JOIN User u ON d.id = u.distributor.id WHERE u.userId = :id AND d.deletedAt IS NULL")
	int countDistributorByIdUser(@Param("id") String id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE distributor d SET d.distributor_name = :#{#distributor.distributorName}, d.address = :#{#distributor.address}," +
			               "d.city = :#{#distributor.city}, d.owner_firstname = :#{#distributor.ownerFirstname}, " +
			               "d.owner_lastname = :#{#distributor.ownerLastname}, d.country = :#{#distributor.country}, d.email = :#{#distributor.email}," +
			               "d.phone = :#{#distributor.phone}, d.fax = :#{#distributor.fax}, d.contact_firstname = :#{#distributor.contactFirstname}, " +
			               "d.contact_lastname = :#{#distributor.contactLastname}, d.contact_phone = :#{#distributor.contactPhone}," +
			               "d.contact_email = :#{#distributor.contactEmail}, d.website = :#{#distributor.website}," +
			               "d.updated_at = :#{#distributor.updatedAt}," +
			               "d.updated_by = :#{#distributor.updatedBy} WHERE d.distributor_id = :id",
			nativeQuery = true)
	void updateDistributor(@Param("distributor") Distributor distributor, @Param("id") String id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardDistributorDto(d.distributorId,d.distributorName, " +
			               "d.address,d.city) FROM Distributor d JOIN Principals p ON d.principal.principalId = p.principalId  WHERE p.principalId LIKE :principalId AND d.deletedAt IS NULL")
	List<DashboardDistributorDto> findDistributorByPrincipalId(String principalId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE distributor SET deleted_at = now() WHERE id = :id", nativeQuery = true)
	void deletedDistributor(@Param("id") int id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.DistributorDto(d.id, d.distributorId, d.distributorName) FROM Distributor d JOIN SalesInvoice s ON d.id = s.distributor.id WHERE s.id = :id")
	DistributorDto findByIdSales(int id);
	
}
