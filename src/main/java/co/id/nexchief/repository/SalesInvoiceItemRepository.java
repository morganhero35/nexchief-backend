package co.id.nexchief.repository;

import co.id.nexchief.dto.SalesInvoiceItemDto;
import co.id.nexchief.model.SalesInvoiceItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalesInvoiceItemRepository extends JpaRepository<SalesInvoiceItem, Integer> {
	
	@Query(value = "SELECT new co.id.nexchief.dto.SalesInvoiceItemDto(i.id, i.quantity, i.subtotal) " +
			               "FROM SalesInvoiceItem i JOIN i.salesInvoice s WHERE s.id = :id")
	List<SalesInvoiceItemDto> findByIdSales(int id);
	
}
