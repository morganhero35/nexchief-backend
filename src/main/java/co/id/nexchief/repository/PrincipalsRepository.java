package co.id.nexchief.repository;

import co.id.nexchief.dto.DashboardPrincipalDto;
import co.id.nexchief.model.Principals;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PrincipalsRepository extends JpaRepository<Principals, Integer> {
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardPrincipalDto(p.principalId,p.principalName,p.address,p.city, p.licensedExpiryDate) " +
			               "FROM Principals p " +
			               "WHERE p.deletedAt IS NULL AND p.licensedExpiryDate > CURRENT_DATE ")
	List<DashboardPrincipalDto> findAllPrincipal();
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardPrincipalDto(p.principalId,p.principalName," +
			               "p.address,p.city, p.licensedExpiryDate)  " +
			               "FROM Principals p " +
			               "WHERE p.deletedAt IS NULL")
	List<DashboardPrincipalDto> findAllPrincipalPage(Pageable pageable);
	
	@Query(value = "SELECT count(p.id) FROM Principals p WHERE p.deletedAt IS NULL")
	int countPrincipals();
	
	@Query(value = "SELECT id, principal_id, principal_name, address, city, country, email, phone, fax, contact_firstname, " +
			               "contact_lastname, contact_phone, contact_email, licensed_expiry_date, created_at, created_by, " +
			               "updated_at, updated_by, deleted_at FROM principals WHERE principal_id = :id", nativeQuery = true)
	Principals findPrincipal(String id);
	
	@Query(value = "SELECT p.id, p.principal_id, p.principal_name, p.address, p.city, p.country, p.email, p.phone, p.fax, p.contact_firstname, " +
			               "p.contact_lastname, p.contact_phone, p.contact_email, p.licensed_expiry_date, p.created_at, p.created_by, " +
			               "p.updated_at, p.updated_by, p.deleted_at FROM `principals` p JOIN `user` u ON p.id = u.principal_id WHERE u.user_id = :id", nativeQuery = true)
	Principals findPrincipalByUserId(String id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardPrincipalDto(p.principalId,p.principalName,p.address,p.city, p.licensedExpiryDate) " +
			               "FROM Principals p WHERE p.principalName LIKE %:name% AND p.deletedAt IS NULL")
	Principals searchPrincipals(String name);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE principals p SET p.deleted_at = :#{#principal.deletedAt} WHERE p.principal_id LIKE :id",
			nativeQuery = true)
	void deletedPrincipal(@Param("principal") Principals principal, @Param("id") String id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE principals p SET p.principal_name = :#{#principal.principalName}, p.address = :#{#principal.address}," +
			               "p.city = :#{#principal.city}, p.country = :#{#principal.country}, p.email = :#{#principal.email}, p.phone = :#{#principal.phone}," +
			               "p.fax = :#{#principal.fax}, p.contact_firstname = :#{#principal.contactFirstname}, " +
			               "p.contact_lastname = :#{#principal.contactLastname}, p.contact_phone = :#{#principal.contactPhone}," +
			               "p.contact_email = :#{#principal.contactEmail}, p.licensed_expiry_date = :#{#principal.licensedExpiryDate}, " +
			               "p.updated_by = :#{#principal.updatedBy},p.updated_at = :#{#principal.updatedAt}  WHERE p.principal_id LIKE :id",
			nativeQuery = true)
	void updatePrincipal(@Param("principal") Principals principal, @Param("id") String id);
	
	@Query(value = "SELECT new co.id.nexchief.dto.DashboardPrincipalDto(p.principalId,p.principalName,p.address,p.city, " +
			               "p.licensedExpiryDate) FROM Principals p " +
			               "WHERE (p.principalName LIKE %:name% OR p.principalId LIKE %:name%) AND p.deletedAt IS NULL")
	List<DashboardPrincipalDto> searchPrincipal(@Param("name") String name);
	
}
