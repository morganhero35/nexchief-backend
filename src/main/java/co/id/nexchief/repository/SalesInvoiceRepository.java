package co.id.nexchief.repository;

import co.id.nexchief.dto.SalesInvoiceDto;
import co.id.nexchief.model.SalesInvoice;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SalesInvoiceRepository extends JpaRepository<SalesInvoice, Integer> {
	
	@Query(value = "SELECT new co.id.nexchief.dto.SalesInvoiceDto(s.transactionDate, s.customer,s.discount,s.tax, s.invoice, s.totalItem, " +
			               "s.transactionStatus) FROM SalesInvoice s WHERE s.user.userId = :id ORDER BY s.transactionDate DESC")
	List<SalesInvoiceDto> findAllSalesInvoiceReport(String id);
	
	
	@Query(value = "SELECT new co.id.nexchief.dto.SalesInvoiceDto(s.id, s.transactionDate, s.customer, s.invoice," +
			               " s.totalItem, s.transactionStatus) FROM SalesInvoice s WHERE s.user.userId = :id " +
			               "AND s.deletedAt IS NULL")
	List<SalesInvoiceDto> pageSalesInvoice(String id, Pageable pageable);
	
	@Query(value = "SELECT new co.id.nexchief.dto.SalesInvoiceDto(s.id, s.transactionDate, s.customer, s.invoice, s.totalItem, s.transactionStatus) " +
			               "FROM SalesInvoice s JOIN s.salesInvoiceItems i " +
			               "WHERE s.customer LIKE %:name% AND s.user.userId = :id AND s.deletedAt IS NULL")
	List<SalesInvoiceDto> searchSalesInvoice(String name, String id);
	
	@Query(value = "SELECT count(s.id) FROM sales_invoice s WHERE (DAY(s.transaction_date) = DAY(CURRENT_DATE())) " +
			               "AND s.user_id = :id AND s.deleted_at IS NULL", nativeQuery = true)
	int countSalesToday(int id);
	
	@Query(value = "SELECT count(s.id) FROM sales_invoice s WHERE MONTH(s.transaction_date) = MONTH(CURRENT_DATE()) AND user_id = :id AND s.deleted_at IS NULL", nativeQuery = true)
	int countSalesMonth(int id);
	
	@Query(value = "SELECT COUNT(s.id) FROM SalesInvoice s WHERE s.user.userId = :id AND s.deletedAt IS NULL")
	int countSales(String id);
	
	@Query("SELECT new co.id.nexchief.dto.SalesInvoiceDto(s.id, s.transactionDate, s.customer, s.discount, s.tax, s.invoice," +
			       "s.totalItem, s.transactionStatus) FROM SalesInvoice s WHERE s.id = :id")
	SalesInvoiceDto findByIdSales(int id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE sales_invoice s SET s.transaction_status = :#{#salesInvoice.transactionStatus} WHERE s.id LIKE :#{#salesInvoice.id}",
			nativeQuery = true)
	void changeSalesInvoice(@Param("salesInvoice") SalesInvoice salesInvoice);
	
}
