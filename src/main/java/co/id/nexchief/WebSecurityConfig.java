package co.id.nexchief;

import co.id.nexchief.filters.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	private final UserDetailsService myUserDetailsService;
	private final JwtRequestFilter jwtRequestFilter;
	
	public WebSecurityConfig(UserDetailsService myUserDetailsService, JwtRequestFilter jwtRequestFilter) {
		this.myUserDetailsService = myUserDetailsService;
		this.jwtRequestFilter = jwtRequestFilter;
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(myUserDetailsService);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable()
				.authorizeRequests()
				.antMatchers("/api/authenticate","/api/admin/insert").permitAll()
				.antMatchers("/api/user", "/api/principal/**", "/api/user/**").hasAnyRole("ADMIN", "USER")
				.antMatchers("/api/product/report/*", "/api/sales/report/*", "/api/user/dashboard", "/api/user/updatePassword/*",
						"/api/product/insert", "/api/product/page", "/api/product/search/*", "/api/product/count",
						"/api/product/*", "/api/product/delete/*", "/api/product/update/*", "/api/product", "/api/distributor/user/*",
						"/api/sales/insert", "/api/sales/page", "/api/sales/search/*", "/api/sales/count", "/api/sales/*",
						"/api/sales/change", "/api/sales/delete/*").hasRole("USER")
				.antMatchers("/api/user/admin/dashboard", "/api/database/backup", "/api/principal",
						"/api/distributor/page", "/api/distributor/*", "/api/distributor/search/*", "/api/distributor/count"
						, "/api/distributor/*", "/api/distributor/update/*", "/api/principal/insert", "/api/principal/page",
						"/api/principal/search/*", "/api/principal/count", "/api/principal/*", "/api/distributor/delete/*",
						"/api/principal/delete/*", "/api/principal/update/*", "/api/distributor", "/api/user/insert",
						"/api/user/page", "/api/user/count", "/api/user/search/*", "/api/user/*", "/api/user/delete/*",
						"/api/user/reset/*", "/api/user/update/*").hasRole("ADMIN")
				.anyRequest().authenticated().and().
				exceptionHandling().and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
		
	}
	
	
}
