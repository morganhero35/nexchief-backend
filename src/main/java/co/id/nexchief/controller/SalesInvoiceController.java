package co.id.nexchief.controller;

import co.id.nexchief.model.SalesInvoice;
import co.id.nexchief.service.SalesInvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api")
public class SalesInvoiceController {
	private final SalesInvoiceService service;
	
	public SalesInvoiceController(SalesInvoiceService service) {
		this.service = service;
	}
	
	@PostMapping("/sales/insert")
	public ResponseEntity<?> insertSalesInvoice(@RequestBody SalesInvoice salesInvoice) {
		return service.insertSales(salesInvoice);
	}
	
	@PostMapping("/sales/change")
	public ResponseEntity<?> changeSalesInvoice(@RequestBody SalesInvoice salesInvoice) {
		return service.changeSalesInvoice(salesInvoice);
	}
	
	@GetMapping("/sales/{id}")
	public ResponseEntity<?> searchSalesInvoice(@PathVariable("id") int id) {
		return service.searchSalesInvoice(id);
	}
	
	@GetMapping("/sales/page")
	public ResponseEntity<?> pageSales(@RequestParam("id") String id, @RequestParam(defaultValue = "0") Integer pageNo,
	                                   @RequestParam(defaultValue = "5") Integer pageSize) {
		return service.pageSalesInvoice(id, pageNo, pageSize);
	}
	
	@GetMapping("/sales/search/{name}")
	public ResponseEntity<?> searchSalesInvoice(@RequestParam("id") String id, @PathVariable("name") String name) {
		return service.searchSalesInvoice(name, id);
	}
	
	@GetMapping("sales/count")
	public ResponseEntity<?> countSalesInvoice(@RequestParam("id") String id) {
		return service.countSalesInvoice(id);
	}
	
	@GetMapping("/sales/report/{id}")
	public ResponseEntity<?> exportSalesInvoice(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
		return service.exportSalesInvoice(response, id);
	}
	
}
