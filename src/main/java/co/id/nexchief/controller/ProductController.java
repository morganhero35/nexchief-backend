package co.id.nexchief.controller;

import co.id.nexchief.model.Product;
import co.id.nexchief.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api")
@Valid
public class ProductController {
	private final ProductService service;
	
	public ProductController(ProductService service) {
		this.service = service;
	}
	
	@GetMapping("/product")
	public ResponseEntity<?> getAllProduct(@RequestParam("id") String id) {
		return service.getAllProduct(id);
	}
	
	@GetMapping("/product/{id}")
	public ResponseEntity<?> getProductById(@PathVariable String id) {
		return service.getProductById(id);
	}
	
	@PostMapping("/product/insert")
	public ResponseEntity<?> insertProduct(@Valid @RequestBody Product product) {
		return service.insertProduct(product);
	}
	
	@PutMapping("/product/update/{id}")
	public ResponseEntity<?> updateProduct(@Valid @RequestBody Product product, @PathVariable(value = "id") String id) {
		return service.updateProduct(product, id);
	}
	
	@GetMapping("/product/search/{name}")
	public ResponseEntity<?> searchProduct(@PathVariable("name") String name, @RequestParam("id") String id) {
		return service.searchProduct(name, id);
	}
	
	@GetMapping("/product/count")
	public ResponseEntity<?> countProduct(@RequestParam("id") String id) {
		return service.countProduct(id);
	}
	
	@GetMapping("/product/page")
	public ResponseEntity<?> pagePrincipal(@RequestParam("id") String id, @RequestParam(defaultValue = "0") Integer pageNo,
	                                    @RequestParam(defaultValue = "5") Integer pageSize) {
		return service.pageProduct(id, pageNo, pageSize);
	}
	
	@PostMapping("/product/delete/{id}")
	public ResponseEntity<?> deleteProduct(@PathVariable("id") String id) {
		return service.deleteProduct(id);
	}
	
	@GetMapping("/product/report/{id}")
	public ResponseEntity<?> exportProduct(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
		return service.exportProduct(response, id);
	}
}
