package co.id.nexchief.controller;

import co.id.nexchief.model.Distributor;
import co.id.nexchief.service.DistributorService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@Validated
public class DistributorController {
	private final DistributorService service;
	
	public DistributorController(DistributorService service) {
		this.service = service;
	}
	
	@GetMapping("/distributor")
	public ResponseEntity<?> getAllDistributor() {
		return service.getAllDistributor();
	}
	
	@GetMapping("/distributor/count")
	public ResponseEntity<?> countDistributor() {
		return service.countDistributor();
	}
	
	@GetMapping("/distributor/page")
	public ResponseEntity<?> pageDistributor(@RequestParam(defaultValue = "0") int pageNo, @RequestParam(defaultValue = "2") Integer pageSize) {
		return service.pageDistributor(pageNo, pageSize);
	}
	
	@GetMapping("/distributor/{id}")
	public ResponseEntity<?> getDistributorById(@PathVariable String id) {
		return service.getDistributorById(id);
	}
	
	@PostMapping("/distributor/insert")
	public ResponseEntity<?> insertDistributor(@Valid @RequestBody Distributor distributor) {
		return service.insertDistributor(distributor);
	}
	
	@PutMapping("/distributor/update/{id}")
	public ResponseEntity<?> updateDistributor(@Valid @RequestBody Distributor distributor, @PathVariable(value = "id") String id) {
		return service.updateDistributor(distributor, id);
	}
	
	@PostMapping("/distributor/delete/{id}")
	public ResponseEntity<?> deleteDistributor(@PathVariable(value = "id") String id) {
		return service.deleteDistributor(id);
	}
	
	@GetMapping("/distributor/search/{name}")
	public ResponseEntity<?> findDistributorByName(@PathVariable(value = "name") String name) {
		return service.searchDistributor(name);
	}
	
	@GetMapping("/distributor/user/{id}")
	public ResponseEntity<?> findDistributorByIdUser(@PathVariable(value = "id") String id) {
		return service.findDistributorByIdUser(id);
	}
	
	@GetMapping("/distributor/principal/{id}")
	public ResponseEntity<?> findDistributorByPrincipalId(@PathVariable(value = "id") String id) {
		return service.findDistributorByPrincipalId(id);
	}
	
}
