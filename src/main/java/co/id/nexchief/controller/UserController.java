package co.id.nexchief.controller;

import co.id.nexchief.model.User;
import co.id.nexchief.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class UserController {
	private final UserService service;
	
	public UserController(UserService service) {
		this.service = service;
	}
	
	@PostMapping("/authenticate")
	public ResponseEntity<?> loginUser(@Valid @RequestBody User user) {
		return service.createAuthenticationToken(user);
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<?> getUserByUserId(@PathVariable String id) {
		return service.getUserByUserId(id);
	}
	
	@GetMapping("/user/cek/{id}")
	public ResponseEntity<?> getUserCheck(@PathVariable String id) {
		return service.getUserCheck(id);
	}
	
	@PostMapping("/user/insert")
	public ResponseEntity<?> insertUser(@Valid @RequestBody User user) {
		return service.registerUser(user);
	}
	
	@PutMapping("/user/update/{id}")
	public ResponseEntity<?> updateUser(@Valid @RequestBody User user, @PathVariable(value = "id") String id) {
		return service.updateUser(user, id);
	}
	
	@GetMapping("/user/count")
	public ResponseEntity<?> countUser() {
		return service.countUser();
	}
	
	@PostMapping("/user/delete/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable("id") String id, @RequestBody User user) {
		return service.deleteUser(id, user);
	}
	
	@GetMapping("/user/search/{name}")
	public ResponseEntity<?> searchUser(@PathVariable("name") String name) {
		return service.searchUser(name);
	}
	
	@GetMapping("/user/page")
	public ResponseEntity<?> pageUser(@RequestParam(defaultValue = "0") Integer pageNo,
	                                  @RequestParam(defaultValue = "5") Integer pageSize) {
		return service.pageUser(pageNo, pageSize);
	}
	
	@PostMapping("/user/updatePassword/{id}")
	public ResponseEntity<?> updatePassword(@Valid @PathVariable("id") String id, @RequestBody User user) {
		
		return service.updatePasswordUser(id, user);
	}
	
	@PutMapping("/user/reset/{id}")
	public ResponseEntity<?> resetPassword(@Valid @PathVariable("id") String id, @RequestBody User user) throws MessagingException{
		return service.resetPassword(id, user);
	}
	
	@GetMapping("/user/dashboard")
	public ResponseEntity<?> dashboardUser(@Valid @RequestParam("id") String id) {
		return service.dashboardUser(id);
	}
	
	@GetMapping("/user/admin/dashboard")
	public ResponseEntity<?> dashboardAdmin() {
		return service.dashboardAdmin();
	}
	
}
