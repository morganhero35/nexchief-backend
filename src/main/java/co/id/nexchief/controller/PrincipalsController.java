package co.id.nexchief.controller;

import co.id.nexchief.dto.DashboardPrincipalDto;
import co.id.nexchief.model.Principals;
import co.id.nexchief.service.PrincipalsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@Valid
public class PrincipalsController {
	private final PrincipalsService service;
	
	public PrincipalsController(PrincipalsService service) {
		this.service = service;
	}
	
	@GetMapping("/principal")
	public List<DashboardPrincipalDto> getAllPrincipal() {
		return service.getAllPrincipal();
	}
	
	@GetMapping("/principal/{id}")
	public ResponseEntity<?> getPrincipalById(@PathVariable String id) {
		return service.getPrincipalById(id);
	}
	
	@PostMapping("/principal/insert")
	public ResponseEntity<?> insertPrincipal(@Valid @RequestBody Principals principals) {
		return service.insertPrincipal(principals);
	}
	
	@PutMapping(value = "/principal/update/{id}")
	public ResponseEntity<?> updatePrincipal(@Valid @RequestBody Principals principals, @PathVariable(value = "id") String id) {
		return service.updatePrincipal(principals, id);
	}
	
	@PostMapping("/principal/delete/{id}")
	public ResponseEntity<?> deletedPrincipal(@PathVariable(value = "id") String id) {
		return service.deletedPrincipal(id);
	}
	
	@GetMapping("/principal/count")
	public ResponseEntity<?> countPrincipal() {
		return service.countPrincipal();
	}
	
	@GetMapping("/principal/page")
	public ResponseEntity<?> pagePrincipal(@RequestParam(defaultValue = "0") Integer pageNo,
	                                       @RequestParam(defaultValue = "5") Integer pageSize) {
		return service.pagePrincipal(pageNo, pageSize);
	}
	
	@GetMapping("/principal/search/{name}")
	public ResponseEntity<?> searchPrincipal(@PathVariable("name") String name) {
		return service.searchPrincipal(name);
	}
	
	@GetMapping("/principal/isActive")
	public ResponseEntity<?> checkPrincipalActive(@RequestParam("id") String id) {
		return service.checkPrincipalActive(id);
	}
}
