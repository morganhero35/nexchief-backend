package co.id.nexchief.controller;


import co.id.nexchief.service.DatabaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/api")
public class DatabaseController {
	private final DatabaseService service;
	
	public DatabaseController(DatabaseService service) {
		this.service = service;
	}
	
	@GetMapping("/database/backup")
	public ResponseEntity<?> backupDatabase(HttpServletResponse response) throws IOException {
		return service.backupDatabase(response);
	}
}
