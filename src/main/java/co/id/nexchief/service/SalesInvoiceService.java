package co.id.nexchief.service;

import co.id.nexchief.dto.*;
import co.id.nexchief.model.*;
import co.id.nexchief.repository.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class SalesInvoiceService {
	private final SalesInvoiceRepository repository;
	private final SalesInvoiceItemRepository itemRepository;
	private final ProductRepository productRepository;
	private final UserRepository userRepository;
	private final PrincipalsRepository principalsRepository;
	private final DistributorRepository distributorRepository;
	
	
	public SalesInvoiceService(SalesInvoiceRepository repository, SalesInvoiceItemRepository itemRepository, ProductRepository productRepository, UserRepository userRepository, PrincipalsRepository principalsRepository, DistributorRepository distributorRepository) {
		this.repository = repository;
		this.itemRepository = itemRepository;
		this.productRepository = productRepository;
		this.userRepository = userRepository;
		this.principalsRepository = principalsRepository;
		this.distributorRepository = distributorRepository;
	}
	
	public ResponseEntity<?> insertSales(SalesInvoice salesInvoice) {
		if (salesInvoice.getDistributor() == null) {
			return new ResponseEntity<>("Distributor is Empty", HttpStatus.BAD_REQUEST);
		}
		User u = userRepository.findByUserId(salesInvoice.getUser().getUserId());
		Principals p = principalsRepository.findPrincipal(u.getPrincipal().getPrincipalId());
		Distributor d = distributorRepository.findDistributorById(salesInvoice.getDistributor().getDistributorId());
		if (p.getDeletedAt() == null && d.getDeletedAt() == null) {
			double totalItem = 0;
			double total;
			for (SalesInvoiceItem item : salesInvoice.getSalesInvoiceItems()) {
				Product product = productRepository.findProductByProductCode(item.getProduct().getProductCode());
				item.setProduct(product);
				item.setSalesInvoice(salesInvoice);
				item.setCreatedAt(new Date());
				item.setCreatedBy(salesInvoice.getUpdatedBy());
				item.setUpdatedAt(new Date());
				item.setUpdatedBy(salesInvoice.getUpdatedBy());
				totalItem = totalItem + item.getSubtotal();
			}
			double tax = (totalItem - salesInvoice.getDiscount()) * 0.1;
			total = totalItem - salesInvoice.getDiscount() + tax;
			salesInvoice.setTax(tax);
			salesInvoice.setCreatedAt(new Date());
			salesInvoice.setUpdatedAt(new Date());
			salesInvoice.setUser(userRepository.findByUserId(salesInvoice.getUser().getUserId()));
			salesInvoice.setTotalItem(totalItem);
			salesInvoice.setInvoice(total);
			if (salesInvoice.getDistributor().getId() == 0) {
				salesInvoice.setDistributor(distributorRepository.findDistributorById(salesInvoice.getDistributor().getDistributorId()));
			}
			repository.save(salesInvoice);
			return new ResponseEntity<>("Sales Invoice Added Successfully", HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity<>("Principal or Distributor Has Been Delete, You Can't Add Sales Invoice", HttpStatus.CONFLICT);
		}
	}
	
	public ResponseEntity<?> changeSalesInvoice(SalesInvoice salesInvoice) {
		User u = userRepository.findByUserId(salesInvoice.getUser().getUserId());
		Principals p = principalsRepository.findPrincipal(u.getPrincipal().getPrincipalId());
		
		if (p.getDeletedAt() == null) {
			salesInvoice.setTransactionStatus("Paid");
			repository.changeSalesInvoice(salesInvoice);
			return new ResponseEntity<>("Sales Invoice Has Change Transaction Status", HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity<>("Principal Has Been Delete, You Can't Change Status Sales Invoice", HttpStatus.CONFLICT);
		}
	}
	
	public ResponseEntity<?> searchSalesInvoice(int id) {
		SalesInvoiceDto salesInvoiceDto = repository.findByIdSales(id);
		List<SalesInvoiceItemDto> list = itemRepository.findByIdSales(id);
		for (SalesInvoiceItemDto salesInvoiceItemDto : list) {
			ProductDto productDto = productRepository.findProductDtoBySalesInvoiceItem(salesInvoiceItemDto.getId());
			salesInvoiceItemDto.setProduct(productDto);
		}
		
		UserDto userDto = userRepository.findUserByIdSales(id);
		DistributorDto distributorDto = distributorRepository.findByIdSales(id);
		
		salesInvoiceDto.setSalesInvoiceItems(list);
		salesInvoiceDto.setUser(userDto);
		salesInvoiceDto.setDistributor(distributorDto);
		return new ResponseEntity<>(salesInvoiceDto, HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> pageSalesInvoice(String id, int pageNo, int pageSize) {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("updatedAt").descending());
		return new ResponseEntity<>(repository.pageSalesInvoice(id, pageable), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> searchSalesInvoice(String name, String id) {
		return new ResponseEntity<>(repository.searchSalesInvoice(name, id), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> countSalesInvoice(String id) {
		return new ResponseEntity<>(repository.countSales(id), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> exportSalesInvoice(HttpServletResponse response, String id) throws IOException {
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=sales_" + currentDateTime + ".csv";
		
		response.setContentType("text/csv");
		response.setHeader(headerKey, headerValue);
		
		List<SalesInvoiceDto> listUsers = repository.findAllSalesInvoiceReport(id);
		
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
		String[] csvHeader = {"Transaction Date", "Customer", "Discount", "Tax", "Invoice",
				"Total Item", "Transaction Status"};
		String[] nameMapping = {"transactionDate", "customer", "discount", "tax", "invoice",
				"totalItem", "transactionStatus"};
		
		csvWriter.writeHeader(csvHeader);
		
		for (SalesInvoiceDto product : listUsers) {
			csvWriter.write(product, nameMapping);
		}
		csvWriter.close();
		return new ResponseEntity<>(response.getWriter(), HttpStatus.ACCEPTED);
	}
}
