package co.id.nexchief.service;

import co.id.nexchief.dto.ProductDto;
import co.id.nexchief.model.Product;
import co.id.nexchief.model.User;
import co.id.nexchief.repository.ProductRepository;
import co.id.nexchief.repository.SalesInvoiceItemRepository;
import co.id.nexchief.repository.UserRepository;
import co.id.nexchief.utils.GenerateId;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
	private final ProductRepository repository;
	private final UserRepository userRepository;
	private final SalesInvoiceItemRepository salesInvoiceItemRepository;
	
	public ProductService(ProductRepository repository, UserRepository userRepository, SalesInvoiceItemRepository salesInvoiceItemRepository) {
		this.repository = repository;
		this.userRepository = userRepository;
		this.salesInvoiceItemRepository = salesInvoiceItemRepository;
	}
	
	public ResponseEntity<?> getAllProduct(String id) {
		List<ProductDto> products = repository.findAllProductSalesByUserId(id);
		return new ResponseEntity<>(products, HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> getProductById(String id) {
		ProductDto product = repository.findProductDtoByProductCode(id);
		return ResponseEntity.ok().body(product);
	}
	
	public ResponseEntity<?> insertProduct(Product product) {
		if (product.getProductCode().isEmpty() || product.getProductCode().isBlank() || product.getProductCode() == null) {
			String nameProduct = product.getProductName().substring(0, 2).toUpperCase();
			String number = GenerateId.idProduct(repository, product.getUser().getUserId());
			String newCode = nameProduct + "-" + number;
			while (true) {
				if (repository.findProductByProductCode(newCode) == null) {
					product.setProductCode(newCode);
					break;
				}
				nameProduct = product.getProductName().substring(1, 3).toUpperCase();
				number = GenerateId.idProduct(repository, product.getUser().getUserId());
				newCode = nameProduct + "-" + number;
			}
		}
		
		if (product.getStandardBuyingPrice() > product.getStandardSellingPrice())
			return new ResponseEntity<>("Standard Selling Price minimum " + product.getStandardBuyingPrice(), HttpStatus.BAD_REQUEST);
		
		if (product.getProductCode().contains(" ")) {
			return new ResponseEntity<>("Input Product Code Without Space!", HttpStatus.BAD_REQUEST);
		}
		
		if (repository.findProductByProductCode(product.getProductCode()) != null) {
			return new ResponseEntity<>("Product Code Already Used", HttpStatus.BAD_REQUEST);
		}
		
		User user = userRepository.findByUserId(product.getUser().getUserId());
		product.setUser(user);
		product.setCreatedAt(new Date());
		product.setUpdatedAt(new Date());
		
		repository.save(product);
		return new ResponseEntity<>("Product Added Successfully", HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> updateProduct(Product product, String id) {
		product.setUser(userRepository.findByUserId(product.getUser().getUserId()));
		product.setUpdatedAt(new Date());
		repository.updateProduct(product, id);
		return new ResponseEntity<>("Product Has Been Updated", HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> deleteProduct(String id) {
		Optional<Product> product = Optional.ofNullable(repository.findProductByProductCode(id));
		
		if (product.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		repository.deletedProduct(product.get());
		return new ResponseEntity<>("Product Has Bee Deleted", HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> searchProduct(String name, String id) {
		return new ResponseEntity<>(repository.searchProducts(name, id), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> countProduct(String id) {
		return new ResponseEntity<>(repository.countProduct(id), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> pageProduct(String id, int pageNo, int pageSize) {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("updatedAt").descending());
		return new ResponseEntity<>(repository.ProductDtoPage(id, pageable), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> exportProduct(HttpServletResponse response, String id) throws IOException {
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=product_" + currentDateTime + ".csv";
		
		response.setContentType("text/csv");
		response.setHeader(headerKey, headerValue);
		
		List<ProductDto> products = repository.productDtoReport(id);
		
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
		String[] csvHeader = {"Product Code", "Product Name", "Packaging", "Product Description", "Product Category",
				"Market Launch Date", "Standard Buying Price", "Standard Selling Price"};
		String[] nameMapping = {"productCode", "productName", "packaging", "productDescription", "productCategory",
				"marketLaunchDate", "standardBuyingPrice", "standardSellingPrice"};
		
		csvWriter.writeHeader(csvHeader);
		
		for (ProductDto product : products) {
			csvWriter.write(product, nameMapping);
		}
		
		csvWriter.close();
		return new ResponseEntity<>(response.getWriter(), HttpStatus.ACCEPTED);
	}
}

