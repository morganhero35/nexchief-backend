package co.id.nexchief.service;

import co.id.nexchief.dto.DashboardPrincipalDto;
import co.id.nexchief.model.Distributor;
import co.id.nexchief.model.Principals;
import co.id.nexchief.model.User;
import co.id.nexchief.repository.DistributorRepository;
import co.id.nexchief.repository.PrincipalsRepository;
import co.id.nexchief.repository.UserRepository;
import co.id.nexchief.utils.GenerateId;
import org.json.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PrincipalsService {
	private final PrincipalsRepository repository;
	private final DistributorRepository distributorRepository;
	private final UserRepository userRepository;
	
	public PrincipalsService(PrincipalsRepository repository, DistributorRepository distributorRepository, UserRepository userRepository) {
		this.repository = repository;
		this.distributorRepository = distributorRepository;
		this.userRepository = userRepository;
	}
	
	public List<DashboardPrincipalDto> getAllPrincipal() {
		return repository.findAllPrincipal();
	}
	
	public ResponseEntity<?> getPrincipalById(String id) {
		Optional<Principals> principal = Optional.ofNullable(repository.findPrincipal(id));
		if (principal.isEmpty())
			return ResponseEntity.notFound().build();
		return new ResponseEntity<>(principal.get(), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<DashboardPrincipalDto> searchPrincipal(String name) {
		return new ResponseEntity(repository.searchPrincipal(name), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<Integer> countPrincipal() {
		return new ResponseEntity<>(repository.countPrincipals(), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<DashboardPrincipalDto> pagePrincipal(int pageNo, int pageSize) {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("updatedAt").descending());
		return new ResponseEntity(repository.findAllPrincipalPage(pageable), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<String> insertPrincipal(Principals principals) {
		if (principals.getPrincipalId().isEmpty() || principals.getPrincipalId().isBlank()
				    || principals.getPrincipalId() == null) {
			String[] firstWordOfPrincipalName = principals.getPrincipalName().split(" ");
			String newId = firstWordOfPrincipalName[0] + GenerateId.idGenerator();
			
			while (true) {
				if (repository.findPrincipal(newId) == null) {
					principals.setPrincipalId(newId.toUpperCase());
					break;
				}
				newId = firstWordOfPrincipalName[0] + GenerateId.idGenerator();
			}
		} else {
			principals.setPrincipalId(principals.getPrincipalId().toUpperCase());
		}
		
		if (principals.getPrincipalId().contains(" ")) {
			return new ResponseEntity<>("Input Principal ID Without Space!", HttpStatus.BAD_REQUEST);
		}
		if (repository.findPrincipal(principals.getPrincipalId()) != null) {
			return new ResponseEntity<>("Principal Already Used", HttpStatus.BAD_REQUEST);
		}
		
		principals.setCreatedAt(new Date());
		principals.setUpdatedAt(new Date());
		
		repository.save(principals);
		return new ResponseEntity<>("Register Principal Successfully", HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<String> updatePrincipal(Principals principals, String id) {
		Optional<Principals> principal = Optional.ofNullable(repository.findPrincipal(id));
		
		if (principal.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		principals.setUpdatedAt(new Date());
		
		repository.updatePrincipal(principals, id);
		return new ResponseEntity<>("Principal Has Been Updated", HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<String> deletedPrincipal(String id) {
		Optional<Principals> principal = Optional.ofNullable(repository.findPrincipal(id));
		
		if (principal.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		Principals principals = repository.findPrincipal(id);
		
		principals.setDeletedAt(new Date());
		repository.deletedPrincipal(principals, id);
		return new ResponseEntity<>("Principal Has Been Deleted", HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<Object> checkPrincipalActive(String id) {
		User u = userRepository.findByUserId(id);
		Principals p = repository.findPrincipal(u.getPrincipal().getPrincipalId());
		Optional<Distributor> optionalDistributor = distributorRepository.findById(u.getDistributor().getId());
		JSONObject object = new JSONObject();
		if (optionalDistributor.isPresent()) {
			if (p.getDeletedAt() != null || optionalDistributor.get().getDeletedAt() != null) {
				object.put("active", true);
			}
		} else {
			object.put("active", p.getDeletedAt() != null);
		}
		
		return new ResponseEntity<>(object.toString(), HttpStatus.ACCEPTED);
	}
}

