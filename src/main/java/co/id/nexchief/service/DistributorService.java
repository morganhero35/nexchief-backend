package co.id.nexchief.service;

import co.id.nexchief.dto.DashboardDistributorDto;
import co.id.nexchief.model.Distributor;
import co.id.nexchief.model.User;
import co.id.nexchief.repository.DistributorRepository;
import co.id.nexchief.repository.PrincipalsRepository;
import co.id.nexchief.repository.UserRepository;
import co.id.nexchief.utils.GenerateId;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class DistributorService {
	private final DistributorRepository repository;
	private final PrincipalsRepository principalsRepository;
	private final UserRepository userRepository;
	
	public DistributorService(DistributorRepository repository, PrincipalsRepository principalsRepository, UserRepository userRepository) {
		this.repository = repository;
		this.principalsRepository = principalsRepository;
		this.userRepository = userRepository;
	}
	
	public ResponseEntity<DashboardDistributorDto> getAllDistributor() {
		return new ResponseEntity(repository.findAllDistributor(), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<Distributor> getDistributorById(String id) {
		Optional<Distributor> distributor = Optional.ofNullable(repository.findDistributorById(id));
		if (distributor.isEmpty())
			return ResponseEntity.notFound().build();
		return new ResponseEntity<>(distributor.get(), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<String> insertDistributor(Distributor distributor) {
		if (distributor.getDistributorId().isEmpty() || distributor.getDistributorId().isBlank()) {
			String newId = GenerateId.idGeneratorForDistributor();
			while (true) {
				if (repository.findDistributorById(newId) == null) {
					distributor.setDistributorId(newId);
					break;
				}
				newId = GenerateId.idGeneratorForDistributor();
			}
		}
		
		if (repository.findDistributorByIdCheck(distributor.getDistributorId()) != null)
			return new ResponseEntity<>("Distributor ID Already Used", HttpStatus.BAD_REQUEST);
		
		if (distributor.getDistributorId().contains(" ")) {
			return new ResponseEntity<>("Input Distributor ID Without Space!", HttpStatus.BAD_REQUEST);
		}
		if (principalsRepository.findPrincipal(distributor.getPrincipal().getPrincipalId()) == null) {
			return new ResponseEntity<>("Principal is Empty", HttpStatus.BAD_REQUEST);
		}
		distributor.setPrincipal(principalsRepository.findPrincipal(distributor.getPrincipal().getPrincipalId()));
		distributor.setCreatedAt(new Date());
		distributor.setUpdatedAt(new Date());
		
		repository.save(distributor);
		return new ResponseEntity<>("Register Distributor Successfully", HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<Object> updateDistributor(Distributor distributor, String id) {
		Optional<Distributor> distributor1 = Optional.ofNullable(repository.findDistributorById(id));
		
		if (distributor1.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		distributor.setUpdatedAt(new Date());
		
		repository.updateDistributor(distributor, id);
		return new ResponseEntity<>("Distributor Has Been Updated", HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<String> deleteDistributor(String id) {
		Optional<Distributor> optionalDistributor = Optional.ofNullable(repository.findDistributorById(id));
		Distributor distributor = optionalDistributor.get();
		repository.deletedDistributor(distributor.getId());
		return new ResponseEntity<>("Distributor Has Been Deleted", HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> searchDistributor(String name) {
		return new ResponseEntity<>(repository.searchDistributor(name), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<Integer> countDistributor() {
		return new ResponseEntity<>(repository.countDistributor(), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<DashboardDistributorDto> pageDistributor(int pageNo, int pageSize) {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("updatedAt").descending());
		return new ResponseEntity(repository.findAllDistributorPage(pageable), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<DashboardDistributorDto> findDistributorByIdUser(String id) {
		User u = userRepository.findByUserId(id);
		if (u.getDistributor() != null) {
			DashboardDistributorDto distributorDto = repository.findDistributorDtoById(u.getDistributor().getDistributorId());
			return new ResponseEntity<>(distributorDto, HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity(repository.findDistributorByPrincipalId(u.getPrincipal().getPrincipalId()), HttpStatus.ACCEPTED);
		}
	}
	
	public ResponseEntity<DashboardDistributorDto> findDistributorByPrincipalId(String id) {
		return new ResponseEntity(repository.findDistributorByPrincipalId(id), HttpStatus.ACCEPTED);
	}
	
}
