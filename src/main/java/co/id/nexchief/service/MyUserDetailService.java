package co.id.nexchief.service;

import co.id.nexchief.model.MyUserDetail;
import co.id.nexchief.model.User;
import co.id.nexchief.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailService implements UserDetailsService {
	final UserRepository userRepository;
	
	public MyUserDetailService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<User> user = Optional.ofNullable(userRepository.findByEmail(email));
		return user.map(MyUserDetail::new).get();
	}
}
