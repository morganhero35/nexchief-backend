package co.id.nexchief.service;

import co.id.nexchief.WebSecurityConfig;
import co.id.nexchief.dto.DashboardUserDto;
import co.id.nexchief.dto.UserDto;
import co.id.nexchief.model.Distributor;
import co.id.nexchief.model.Principals;
import co.id.nexchief.model.User;
import co.id.nexchief.repository.*;
import co.id.nexchief.utils.GenerateId;
import co.id.nexchief.utils.JwtUtil;
import lombok.SneakyThrows;
import org.json.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.Optional;

@Service
public class UserService {
	private final UserRepository repository;
	private final PrincipalsRepository principalsRepository;
	private final DistributorRepository distributorRepository;
	private final SalesInvoiceRepository salesInvoiceRepository;
	private final ProductRepository productRepository;
	private final JwtUtil jwtTokenUtil;
	private final AuthenticationManager authenticationManager;
	private final MyUserDetailService userDetailsService;
	private final WebSecurityConfig encode;
	private final EmailServices emailServices;
	
	public UserService(UserRepository repository, PrincipalsRepository principalsRepository, DistributorRepository distributorRepository, SalesInvoiceRepository salesInvoiceRepository, ProductRepository productRepository, JwtUtil jwtTokenUtil, AuthenticationManager authenticationManager, MyUserDetailService userDetailsService, WebSecurityConfig encode, EmailServices emailServices) {
		this.repository = repository;
		this.principalsRepository = principalsRepository;
		this.distributorRepository = distributorRepository;
		this.salesInvoiceRepository = salesInvoiceRepository;
		this.productRepository = productRepository;
		this.jwtTokenUtil = jwtTokenUtil;
		this.authenticationManager = authenticationManager;
		this.userDetailsService = userDetailsService;
		this.encode = encode;
		this.emailServices = emailServices;
	}
	
	
	public ResponseEntity<?> createAuthenticationToken(@RequestBody User users) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken
						              (users.getEmail(), users.getPassword()));
		UserDetails userDetails = userDetailsService
				.loadUserByUsername(users.getEmail());
		UserDto userDto = repository.findUserDtoByEmail(users.getEmail());
		final String jwt = jwtTokenUtil.generateToken(userDetails);
		userDto.setJwt(jwt);
		if (userDto.getRoles().equalsIgnoreCase("ROLE_USER")) {
			User user = repository.findByUserIdLogin(userDto.getUserId());
			userDto.setPrincipal(user.getPrincipal());
			if (userDto.isDisableLogin()
					    || userDto.getProductValidThru().before(new Date())
					    || userDto.getPrincipal().getLicensedExpiryDate().before(new Date())
					    || user.getDeletedAt() != null) {
				return new ResponseEntity<>("Account User is Disable or Expired", HttpStatus.FORBIDDEN);
			}
		}
		return new ResponseEntity<>(userDto, HttpStatus.OK);
	}
	
	
	public ResponseEntity<?> getUserByUserId(String id) {
		Optional<User> user = Optional.ofNullable(repository.findByUserId(id));
		if (user.isEmpty())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(user);
	}
	
	public ResponseEntity getUserCheck(String id) {
		UserDto userDto = repository.findUserDtoByUserId(id);
		return ResponseEntity.ok().body(userDto);
	}
	
	
	public ResponseEntity<?> searchUser(String name) {
		return new ResponseEntity<>(repository.searchUser(name), HttpStatus.ACCEPTED);
	}
	
	
	@SneakyThrows
	public ResponseEntity<?> registerUser(User user) {
		String pass;
		String newId = user.getUserId().toUpperCase() + "." + user.getPrincipal().getPrincipalId();
		user.setUserId(newId);
		
		if (repository.findByUserIdCheck(user.getUserId()) != null)
			return new ResponseEntity<>("User ID Already Used", HttpStatus.BAD_REQUEST);
		
		if (user.getUserId().contains(" "))
			return new ResponseEntity<>("Input User ID Without Space!", HttpStatus.BAD_REQUEST);
		
		if (repository.findByEmail(user.getEmail()) != null)
			return new ResponseEntity<>("Email Already Used", HttpStatus.BAD_REQUEST);
		
		if (user.getPassword().isEmpty())
			return new ResponseEntity<>("Password Must Be Input!", HttpStatus.BAD_REQUEST);
		
		if (principalsRepository.findPrincipal(user.getPrincipal().getPrincipalId()) == null) {
			return new ResponseEntity<>("Principal is Empty", HttpStatus.BAD_REQUEST);
		}
		
		Principals principal = principalsRepository.findPrincipal(user.getPrincipal().getPrincipalId());
		user.setPrincipal(principal);
		
		if (user.getDistributor() != null) {
			Distributor distributor = distributorRepository.findDistributorById(user.getDistributor().getDistributorId());
			user.setDistributor(distributor);
		} else {
			user.setDistributor(null);
		}
		
		user.setRegistrationDate(new Date());
		user.setCreatedAt(new Date());
		user.setUpdatedAt(new Date());
		user.setRoles("ROLE_USER");
		pass = user.getPassword();
		
		user.setPassword(encode.passwordEncoder().encode(pass));
		repository.save(user);
		emailServices.registerUser(user, pass);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> updateUser(User user, String id) {
		User user1 = repository.findByUserIdCheck(id);
		if (repository.findByUserIdCheck(id) == null)
			return new ResponseEntity<>("User ID Already Used", HttpStatus.BAD_REQUEST);
		
		if (principalsRepository.findPrincipal(user.getPrincipal().getPrincipalId()) == null) {
			return new ResponseEntity<>("Principal is Empty", HttpStatus.BAD_REQUEST);
		}
		
		user.setUserId(user1.getUserId());
		user.setEmail(user1.getEmail());
		user.setPassword(user1.getPassword());
		user.setUpdatedAt(new Date());
		user.setPrincipal(user1.getPrincipal());
		user.setDistributor(user1.getDistributor());
		repository.updateUser(user, id);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> updatePasswordUser(String id, User u) {
		Optional<User> optionalUser = Optional.ofNullable(repository.findByUserId(id));
		if (optionalUser.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		if (u.getPassword().matches("^(?=.*\\d)(?=.*[A-Z])(?=.*[a-z]).{8,}$")) {
			User user = optionalUser.get();
			user.setPassword(encode.passwordEncoder().encode(u.getPassword()));
			user.setUpdatedBy(u.getUpdatedBy());
			user.setUpdatedAt(new Date());
			repository.updatePasswordUser(user, id);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity<>("Minimum 8 characters, at least one uppercase letter, one lowercase letter, one number.", HttpStatus.BAD_REQUEST);
		}
	}
	
	public ResponseEntity<?> deleteUser(String id, User users) {
		Optional<User> optionalUser = Optional.ofNullable(repository.findByUserId(id));
		try {
			if (optionalUser.isEmpty()) {
				return ResponseEntity.notFound().build();
			}
			User user = optionalUser.get();
			user.setUpdatedAt(new Date());
			user.setUpdatedBy(users.getUpdatedBy());
			user.setDeletedAt(new Date());
			repository.deleteById(user, id);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_MODIFIED);
		}
	}
	
	public ResponseEntity<?> countUser() {
		return new ResponseEntity<>(repository.countUser(), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<DashboardUserDto> pageUser(int pageNo, int pageSize) {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("updatedAt").descending());
		return new ResponseEntity(repository.pageUser(pageable), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> resetPassword(String id, User users) throws MessagingException {
		User user = repository.findByUserId(id);
		user.setUpdatedAt(new Date());
		user.setUpdatedBy(users.getUpdatedBy());
		String pass = GenerateId.getPassword(8);
		user.setPassword(encode.passwordEncoder().encode(pass));
		emailServices.resetPassword(user, pass);
		repository.updatePasswordUser(user, id);
		return new ResponseEntity<>("Password User " + user.getName() + " Has Been Reset",
				HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> dashboardUser(String id) {
		User user = repository.findByUserId(id);
		JSONObject object = new JSONObject();
		object.put("today", salesInvoiceRepository.countSalesToday(user.getId()));
		object.put("month", salesInvoiceRepository.countSalesMonth(user.getId()));
		object.put("product", productRepository.countProduct(id));
		if (user.getDistributor() != null) {
			object.put("distributor", distributorRepository.countDistributorByIdUser(user.getUserId()));
		} else {
			object.put("distributor", distributorRepository.countDistributorByIdPrincipal(user.getPrincipal().getPrincipalId()));
		}
		return new ResponseEntity<>(object.toString(), HttpStatus.ACCEPTED);
	}
	
	public ResponseEntity<?> dashboardAdmin() {
		JSONObject object = new JSONObject();
		object.put("principal", principalsRepository.count());
		object.put("distributor", distributorRepository.count());
		object.put("user", repository.countUser());
		return new ResponseEntity<>(object.toString(), HttpStatus.ACCEPTED);
	}
	
}
