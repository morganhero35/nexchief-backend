package co.id.nexchief.service;

import co.id.nexchief.model.User;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class EmailServices {
	private final JavaMailSender emailSender;
	
	public EmailServices(JavaMailSender emailSender) {
		this.emailSender = emailSender;
	}
	
	public void registerUser(User user, String password) throws MessagingException {
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(user.getEmail());
		helper.setSubject("Registration your account, " + user.getName());
		helper.setText("Hello " + user.getName() + ", \n" +
				               "You can login in nexchief application with your account: \n" +
				               "Email    : " + user.getEmail() + " \n" +
				               "Password : " + password + " \n");
		emailSender.send(message);
	}
	
	public void resetPassword(User user, String password) throws MessagingException {
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(user.getEmail());
		helper.setSubject("Reset Password    " + user.getName());
		helper.setText("Hello " + user.getName() + ", \n" +
				               "Your password has been reset by " + user.getUpdatedBy() + " \n" +
				               "Email    : " + user.getEmail() + " \n" +
				               "Password : " + password + " \n");
		emailSender.send(message);
	}
	
	
}
