package co.id.nexchief.utils;

import co.id.nexchief.repository.ProductRepository;

import java.security.SecureRandom;
import java.util.Random;

public class GenerateId {
	public static String idGenerator() {
		Random r = new Random();
		StringBuilder output = new StringBuilder();
		for (int i = 0; i < 3; i++) {
			output.append(r.nextInt(10));
		}
		return output.toString();
	}
	
	public static String idProduct(ProductRepository repository, String UserId) {
		String code = String.valueOf(repository.findAllProductByUserId(UserId).size() + 1);
		String id = "";
		if (code.length() == 1) {
			id = "000" + code;
		} else if (code.length() == 2) {
			id = "00" + code;
		} else if (code.length() == 3) {
			id = "0" + code;
		} else if (code.length() == 4) {
			id = code;
		}
		return id;
	}
	
	public static String idGeneratorForDistributor() {
		Random r = new Random();
		StringBuilder output = new StringBuilder();
		for (int i = 0; i < 7; i++) {
			output.append(r.nextInt(10));
		}
		return output.toString();
	}
	
	public static String getPassword(int length) {
		char[] LOWERCASE = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		char[] UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		char[] NUMBERS = "0123456789".toCharArray();
		char[] ALL_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
		assert length >= 3;
		char[] password = new char[length];
		Random rand = new SecureRandom();
		
		password[0] = LOWERCASE[rand.nextInt(LOWERCASE.length)];
		password[1] = UPPERCASE[rand.nextInt(UPPERCASE.length)];
		password[2] = NUMBERS[rand.nextInt(NUMBERS.length)];
		
		
		for (int i = 3; i < length; i++) {
			password[i] = ALL_CHARS[rand.nextInt(ALL_CHARS.length)];
		}
		
		
		for (int i = 0; i < password.length; i++) {
			int randomPosition = rand.nextInt(password.length);
			char temp = password[i];
			password[i] = password[randomPosition];
			password[randomPosition] = temp;
		}
		
		return new String(password);
	}
}
