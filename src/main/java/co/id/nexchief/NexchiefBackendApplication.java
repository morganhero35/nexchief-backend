package co.id.nexchief;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NexchiefBackendApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(NexchiefBackendApplication.class, args);
	}
	
}
