package co.id.nexchief;

import co.id.nexchief.service.DatabaseService;
import com.google.common.hash.Hashing;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

@SpringBootTest
class NexchiefBackendApplicationTests {
	@Autowired
	DatabaseService databaseService;
	
	@Test
	void contextLoads() throws IOException {
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=backup_nexchief_" + currentDateTime + ".sql";
//
//		response.setContentType("text/plain");
//		response.setHeader(headerKey, headerValue);
		
		String dbUsername = "root";
		String dbPassword = "root";
		String dbName = "nexchief";
		String outputFile = "nexchief.sql";
		String command = String.format("mysqldump -u%s -p%s --databases %s > %s",
				dbUsername, dbPassword, dbName, outputFile);
		
		Process process = Runtime.getRuntime().exec(command);
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuilder text = new StringBuilder();
		String s;
		while ((s = stdInput.readLine()) != null) {
			text.append(s).append("\n");
		}
		
		String encodedString = Base64.getEncoder().encodeToString(text.toString().getBytes());
		System.out.println(encodedString + "\n\n");
		
	}
	
}
